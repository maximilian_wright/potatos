#include "idt.h"
#include "util/mem.h"
#include "io/io.h"
#include "io/stdio.h"
#include "util/num.h"

IDTEntry idt[256];
IDTPointer idtp;

extern "C" void isr0();
extern "C" void isr1();
extern "C" void isr2();
extern "C" void isr3();
extern "C" void isr4();
extern "C" void isr5();
extern "C" void isr6();
extern "C" void isr7();
extern "C" void isr8();
extern "C" void isr9();
extern "C" void isr10();
extern "C" void isr11();
extern "C" void isr12();
extern "C" void isr13();
extern "C" void isr14();
extern "C" void isr15();
extern "C" void isr16();
extern "C" void isr17();
extern "C" void isr18();
extern "C" void isr19();
extern "C" void isr20();
extern "C" void isr21();
extern "C" void isr22();
extern "C" void isr23();
extern "C" void isr24();
extern "C" void isr25();
extern "C" void isr26();
extern "C" void isr27();
extern "C" void isr28();
extern "C" void isr29();
extern "C" void isr30();
extern "C" void isr31();

extern "C" void irq0();
extern "C" void irq1();
extern "C" void irq2();
extern "C" void irq3();
extern "C" void irq4();
extern "C" void irq5();
extern "C" void irq6();
extern "C" void irq7();
extern "C" void irq8();
extern "C" void irq9();
extern "C" void irq10();
extern "C" void irq11();
extern "C" void irq12();
extern "C" void irq13();
extern "C" void irq14();
extern "C" void irq15();

extern "C" void flushIDT(uint);

void setIDTGate(byte index, uint base, ushort sel, byte flags)
{
	idt[index].baseLow = base & 0xFFFF;
	idt[index].baseHigh = (base >> 16) & 0xFFFF;
	
	idt[index].sel = sel;
	idt[index].zero = 0;
	
	idt[index].flags = flags; // | 0x60;
}

void initIDT()
{
	idtp.limit = sizeof(IDTEntry) * 256 - 1;
	idtp.base = (uint)&idt;
	
	memset(&idt, 0, sizeof(IDTEntry) * 256);

	outb(PIC_MASTER_CMD, 0x11);
	outb(PIC_SLAVE_CMD, 0x11);
	outb(PIC_MASTER_DATA, 0x20);
	outb(PIC_SLAVE_DATA, 0x28);
	outb(PIC_MASTER_DATA, 4);
	outb(PIC_SLAVE_DATA, 2);
	outb(PIC_MASTER_DATA, 0x01);
	outb(PIC_SLAVE_DATA, 0x01);

	//set masks
	outb(PIC_MASTER_DATA, 255);
	outb(PIC_SLAVE_DATA, 255);
	
	setIDTGate(0, (uint)isr0, 0x08, 0x8E);
	setIDTGate(1, (uint)isr1, 0x08, 0x8E);
	setIDTGate(2, (uint)isr2, 0x08, 0x8E);
	setIDTGate(3, (uint)isr3, 0x08, 0x8E);
	setIDTGate(4, (uint)isr4, 0x08, 0x8E);
	setIDTGate(5, (uint)isr5, 0x08, 0x8E);
	setIDTGate(6, (uint)isr6, 0x08, 0x8E);
	setIDTGate(7, (uint)isr7, 0x08, 0x8E);
	setIDTGate(8, (uint)isr8, 0x08, 0x8E);
	setIDTGate(9, (uint)isr9, 0x08, 0x8E);
	setIDTGate(10, (uint)isr10, 0x08, 0x8E);
	setIDTGate(11, (uint)isr11, 0x08, 0x8E);
	setIDTGate(12, (uint)isr12, 0x08, 0x8E);
	setIDTGate(13, (uint)isr13, 0x08, 0x8E);
	setIDTGate(14, (uint)isr14, 0x08, 0x8E);
	setIDTGate(15, (uint)isr15, 0x08, 0x8E);
	setIDTGate(16, (uint)isr16, 0x08, 0x8E);
	setIDTGate(17, (uint)isr17, 0x08, 0x8E);
	setIDTGate(18, (uint)isr18, 0x08, 0x8E);
	setIDTGate(19, (uint)isr19, 0x08, 0x8E);
	setIDTGate(20, (uint)isr20, 0x08, 0x8E);
	setIDTGate(21, (uint)isr21, 0x08, 0x8E);
	setIDTGate(22, (uint)isr22, 0x08, 0x8E);
	setIDTGate(23, (uint)isr23, 0x08, 0x8E);
	setIDTGate(24, (uint)isr24, 0x08, 0x8E);
	setIDTGate(25, (uint)isr25, 0x08, 0x8E);
	setIDTGate(26, (uint)isr26, 0x08, 0x8E);
	setIDTGate(27, (uint)isr27, 0x08, 0x8E);
	setIDTGate(28, (uint)isr28, 0x08, 0x8E);
	setIDTGate(29, (uint)isr29, 0x08, 0x8E);
	setIDTGate(30, (uint)isr30, 0x08, 0x8E);
	setIDTGate(31, (uint)isr31, 0x08, 0x8E);

	setIDTGate(32, (uint)irq0, 0x08, 0x8E);
	setIDTGate(33, (uint)irq1, 0x08, 0x8E);
	setIDTGate(34, (uint)irq2, 0x08, 0x8E);
	setIDTGate(35, (uint)irq3, 0x08, 0x8E);
	setIDTGate(36, (uint)irq4, 0x08, 0x8E);
	setIDTGate(37, (uint)irq5, 0x08, 0x8E);
	setIDTGate(38, (uint)irq6, 0x08, 0x8E);
	setIDTGate(39, (uint)irq7, 0x08, 0x8E);
	setIDTGate(40, (uint)irq8, 0x08, 0x8E);
	setIDTGate(41, (uint)irq9, 0x08, 0x8E);
	setIDTGate(42, (uint)irq10, 0x08, 0x8E);
	setIDTGate(43, (uint)irq11, 0x08, 0x8E);
	setIDTGate(44, (uint)irq12, 0x08, 0x8E);
	setIDTGate(45, (uint)irq13, 0x08, 0x8E);
	setIDTGate(46, (uint)irq14, 0x08, 0x8E);
	setIDTGate(47, (uint)irq15, 0x08, 0x8E);
	
	flushIDT((uint)&idtp);
}

void showMasks()
{
	char text[8];
	itob(inb(PIC_MASTER_DATA), text);
	writef("Master mask: %s\n", text);
	itob(inb(PIC_SLAVE_DATA), text);
	writef("Slave mask: %s\n", text);
}