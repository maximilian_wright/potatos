#ifndef __VGA_H__
#define __VGA_H__

#include "types.h"
#include "evgacolor.h"

static const byte VGA_WIDTH = 80;
static const byte VGA_HEIGHT = 25;

static inline byte vgaEntryColor(EVGAColor fg, EVGAColor bg)
{
	return fg | bg << 4;
}

static inline ushort vgaEntry(byte uc, byte color)
{
	return (ushort)uc | (ushort)color << 8;
}

#endif
