#include "paging.h"
#include "types.h"
#include "util/mem.h"

uint directory[1024]
__attribute__((aligned(4096)));

uint tables[1024]
__attribute__((aligned(4096)));

extern "C" void loadPageDirectory(uint*);
extern "C" void enablePaging();

void initPaging()
{
    memset(directory, 0, 1024);

    for (int i = 0; i < 1024; i++)
    {
        tables[i] = (i * 0x1000) | PT_READ_WRITE | PT_PRESENT;
    }

    directory[0] = (uint)(&tables) | PT_READ_WRITE | PT_PRESENT;

    loadPageDirectory(directory);
    enablePaging();
}