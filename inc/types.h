#ifndef __TYPES_H__
#define __TYPES_H__

#include <stdint.h>

typedef uint8_t byte;

typedef uint16_t ushort;
typedef uint32_t uint;
typedef uint64_t ulong;

typedef void (*Function)();

#endif