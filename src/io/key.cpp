#include "io/io.h"
#include "io/key.h"
#include "io/stdio.h"
#include "util/num.h"
#include "irq.h"
#include "idt.h"
#include "io/keyevent.h"
#include "time.h"

bool keystate[EKey::NumKeys];
bool capsLockOn = false;
bool scrollLockOn = false;
bool numLockOn = true;
EScanSet scanSet = EScanSet::One;

void initKey()
{
    registerIrq(IRQ1, (Function)irqKeyboard);

    writef("Disabling key scanning...");
    disableKeyInterrupts();
    writeln("OK");

    // outb(KEY_CMD_PORT, KEY_CMD_SCANCODE);
    // outb(KEY_DATA_PORT, SCANCODE_GET);
    // byte response = readScancode();
    // writef("response: %h\n", response);

    // writeln("sleeping 2000");
    // sleep(2000);
    // writeln("done sleeping");

    // byte set = getScancodeSet();
    // writef("Scancode set: %i\n", set);

    // writef("Enabling key scanning...");
    // enableKeyInterrupts();
    // writeln("OK");

    enableInterrupt(IRQ1 - 32);
}

void disableKeyInterrupts()
{
    byte response;

    do
    {
        outb(KEY_CMD_PORT, KEY_CMD_DISABLE_SCAN);
        response = readScancode();
    }
    while (response != KEY_ACK);
}

void enableKeyInterrupts()
{
    byte response;

    do
    {
        outb(KEY_CMD_PORT, KEY_CMD_ENABLE_SCAN);
        response = readScancode();
    }
    while (response != KEY_ACK);
}

byte getScancodeSet()
{
    byte response;
    outb(KEY_CMD_PORT, KEY_CMD_SCANCODE);

    outb(KEY_DATA_PORT, SCANCODE_GET);

    response = readScancode();
    writef("cmd_scancode response: %h\n", response);

    return 0;//readScancode();
}

void setScancodeSet(EScanSet set)
{

}

void irqKeyboard()
{
    KeyEvent e;
    e.pressed = true;

    switch (scanSet)
    {
        case EScanSet::One: readScanSet1(&e.key, &e.pressed);
            break;
        case EScanSet::Two: readScanSet2(&e.key, &e.pressed);
            break;
        case EScanSet::Three: readScanSet3(&e.key, &e.pressed);
            break;
    }

    keystate[(int)e.key] = e.pressed;

    switch (e.key)
    {
        case EKey::CapsLock:
            capsLockOn = !capsLockOn;
            break;
        case EKey::NumLock:
            numLockOn = !numLockOn;
            break;
        case EKey::ScrollLock:
            scrollLockOn = !scrollLockOn;
            break;
    }

    if (e.pressed && hasChar(e.key))
    {
        write(getChar(e.key));
    }

    //writef("%s %s\n", getName(e.key), e.pressed ? "pressed" : "released");
}

void readScanSet1(EKey* key, bool* press)
{
	byte scan = readScancode();
    byte i = 0;

    switch (scan)
    {
        case 0x01: *key = EKey::Escape; break;
        case 0x81: *key = EKey::Escape; *press = false; break;
        case 0x3B: *key = EKey::F1; break;
        case 0xBB: *key = EKey::F1; *press = false; break;
        case 0x3C: *key = EKey::F2; break;
        case 0xBC: *key = EKey::F2; *press = false; break;
        case 0x3D: *key = EKey::F3; break;
        case 0xBD: *key = EKey::F3; *press = false; break;
        case 0x3E: *key = EKey::F4; break;
        case 0xBE: *key = EKey::F4; *press = false; break;
        case 0x3F: *key = EKey::F5; break;
        case 0xBF: *key = EKey::F5; *press = false; break;
        case 0x40: *key = EKey::F6; break;
        case 0xC0: *key = EKey::F6; *press = false; break;
        case 0x41: *key = EKey::F7; break;
        case 0xC1: *key = EKey::F7; *press = false; break;
        case 0x42: *key = EKey::F8; break;
        case 0xC2: *key = EKey::F8; *press = false; break;
        case 0x43: *key = EKey::F9; break;
        case 0xC3: *key = EKey::F9; *press = false; break;
        case 0x44: *key = EKey::F10; break;
        case 0xC4: *key = EKey::F10; *press = false; break;
        case 0x57: *key = EKey::F11; break;
        case 0xD7: *key = EKey::F11; *press = false; break;
        case 0x58: *key = EKey::F12; break;
        case 0xD8: *key = EKey::F12; *press = false; break;
        case 0x29: *key = EKey::Grave; break;
        case 0xA9: *key = EKey::Grave; *press = false; break;
        case 0x02: *key = EKey::Number1; break;
        case 0x82: *key = EKey::Number1; *press = false; break;
        case 0x03: *key = EKey::Number2; break;
        case 0x83: *key = EKey::Number2; *press = false; break;
        case 0x04: *key = EKey::Number3; break;
        case 0x84: *key = EKey::Number3; *press = false; break;
        case 0x05: *key = EKey::Number4; break;
        case 0x85: *key = EKey::Number4; *press = false; break;
        case 0x06: *key = EKey::Number5; break;
        case 0x86: *key = EKey::Number5; *press = false; break;
        case 0x07: *key = EKey::Number6; break;
        case 0x87: *key = EKey::Number6; *press = false; break;
        case 0x08: *key = EKey::Number7; break;
        case 0x88: *key = EKey::Number7; *press = false; break;
        case 0x09: *key = EKey::Number8; break;
        case 0x89: *key = EKey::Number8; *press = false; break;
        case 0x0A: *key = EKey::Number9; break;
        case 0x8A: *key = EKey::Number9; *press = false; break;
        case 0x0B: *key = EKey::Number0; break;
        case 0x8B: *key = EKey::Number0; *press = false; break;
        case 0x0C: *key = EKey::Minus; break;
        case 0x8C: *key = EKey::Minus; *press = false; break;
        case 0x0D: *key = EKey::Equals; break;
        case 0x8D: *key = EKey::Equals; *press = false; break;
        case 0x0E: *key = EKey::Backspace; break;
        case 0x8E: *key = EKey::Backspace; *press = false; break;
        case 0x0F: *key = EKey::Tab; break;
        case 0x8F: *key = EKey::Tab; *press = false; break;
        case 0x10: *key = EKey::Q; break;
        case 0x90: *key = EKey::Q; *press = false; break;
        case 0x11: *key = EKey::W; break;
        case 0x91: *key = EKey::W; *press = false; break;
        case 0x12: *key = EKey::E; break;
        case 0x92: *key = EKey::E; *press = false; break;
        case 0x13: *key = EKey::R; break;
        case 0x93: *key = EKey::R; *press = false; break;
        case 0x14: *key = EKey::T; break;
        case 0x94: *key = EKey::T; *press = false; break;
        case 0x15: *key = EKey::Y; break;
        case 0x95: *key = EKey::Y; *press = false; break;
        case 0x16: *key = EKey::U; break;
        case 0x96: *key = EKey::U; *press = false; break;
        case 0x17: *key = EKey::I; break;
        case 0x97: *key = EKey::I; *press = false; break;
        case 0x18: *key = EKey::O; break;
        case 0x98: *key = EKey::O; *press = false; break;
        case 0x19: *key = EKey::P; break;
        case 0x99: *key = EKey::P; *press = false; break;
        case 0x1A: *key = EKey::LeftBracket; break;
        case 0x9A: *key = EKey::LeftBracket; *press = false; break;
        case 0x1B: *key = EKey::RightBracket; break;
        case 0x9B: *key = EKey::RightBracket; *press = false; break;
        case 0x2B: *key = EKey::Backslash; break;
        case 0xAB: *key = EKey::Backslash; *press = false; break;
        case 0x3A: *key = EKey::CapsLock; break;
        case 0xBA: *key = EKey::CapsLock; *press = false; break;
        case 0x1E: *key = EKey::A; break;
        case 0x9E: *key = EKey::A; *press = false; break;
        case 0x1F: *key = EKey::S; break;
        case 0x9F: *key = EKey::S; *press = false; break;
        case 0x20: *key = EKey::D; break;
        case 0xA0: *key = EKey::D; *press = false; break;
        case 0x21: *key = EKey::F; break;
        case 0xA1: *key = EKey::F; *press = false; break;
        case 0x22: *key = EKey::G; break;
        case 0xA2: *key = EKey::G; *press = false; break;
        case 0x23: *key = EKey::H; break;
        case 0xA3: *key = EKey::H; *press = false; break;
        case 0x24: *key = EKey::J; break;
        case 0xA4: *key = EKey::J; *press = false; break;
        case 0x25: *key = EKey::K; break;
        case 0xA5: *key = EKey::K; *press = false; break;
        case 0x26: *key = EKey::L; break;
        case 0xA6: *key = EKey::L; *press = false; break;
        case 0x27: *key = EKey::Semicolon; break;
        case 0xA7: *key = EKey::Semicolon; *press = false; break;
        case 0x28: *key = EKey::Apostrophe; break;
        case 0xA8: *key = EKey::Apostrophe; *press = false; break;
        case 0x1C: *key = EKey::Enter; break;
        case 0x9C: *key = EKey::Enter; *press = false; break;
        case 0x2A: *key = EKey::LeftShift; break;
        case 0xAA: *key = EKey::LeftShift; *press = false; break;
        case 0x2C: *key = EKey::Z; break;
        case 0xAC: *key = EKey::Z; *press = false; break;
        case 0x2D: *key = EKey::X; break;
        case 0xAD: *key = EKey::X; *press = false; break;
        case 0x2E: *key = EKey::C; break;
        case 0xAE: *key = EKey::C; *press = false; break;
        case 0x2F: *key = EKey::V; break;
        case 0xAF: *key = EKey::V; *press = false; break;
        case 0x30: *key = EKey::B; break;
        case 0xB0: *key = EKey::B; *press = false; break;
        case 0x31: *key = EKey::N; break;
        case 0xB1: *key = EKey::N; *press = false; break;
        case 0x32: *key = EKey::M; break;
        case 0xB2: *key = EKey::M; *press = false; break;
        case 0x33: *key = EKey::Comma; break;
        case 0xB3: *key = EKey::Comma; *press = false; break;
        case 0x34: *key = EKey::Period; break;
        case 0xB4: *key = EKey::Period; *press = false; break;
        case 0x35: *key = EKey::Slash; break;
        case 0xB5: *key = EKey::Slash; *press = false; break;
        case 0x36: *key = EKey::RightShift; break;
        case 0xB6: *key = EKey::RightShift; *press = false; break;
        case 0x1D: *key = EKey::LeftControl; break;
        case 0x9D: *key = EKey::LeftControl; *press = false; break;
        case 0x38: *key = EKey::LeftAlt; break;
        case 0xB8: *key = EKey::LeftAlt; *press = false; break;
        case 0x39: *key = EKey::Space; break;
        case 0xB9: *key = EKey::Space; *press = false; break;
        case 0x46: *key = EKey::ScrollLock; break;
        case 0xC6: *key = EKey::ScrollLock; *press = false; break;
        case 0x45: *key = EKey::NumLock; break;
        case 0xC5: *key = EKey::NumLock; *press = false; break;
        case 0x37: *key = EKey::KeypadMultiply; break;
        case 0xB7: *key = EKey::KeypadMultiply; *press = false; break;
        case 0x4E: *key = EKey::KeypadAdd; break;
        case 0xCE: *key = EKey::KeypadAdd; *press = false; break;
        case 0x4A: *key = EKey::KeypadSubtract; break;
        case 0xCA: *key = EKey::KeypadSubtract; *press = false; break;
        case 0x52: *key = EKey::Keypad0; break;
        case 0xD2: *key = EKey::Keypad0; *press = false; break;
        case 0x4F: *key = EKey::Keypad1; break;
        case 0xCF: *key = EKey::Keypad1; *press = false; break;
        case 0x50: *key = EKey::Keypad2; break;
        case 0xD0: *key = EKey::Keypad2; *press = false; break;
        case 0x51: *key = EKey::Keypad3; break;
        case 0xD1: *key = EKey::Keypad3; *press = false; break;
        case 0x4B: *key = EKey::Keypad4; break;
        case 0xCB: *key = EKey::Keypad4; *press = false; break;
        case 0x4C: *key = EKey::Keypad5; break;
        case 0xCC: *key = EKey::Keypad5; *press = false; break;
        case 0x4D: *key = EKey::Keypad6; break;
        case 0xCD: *key = EKey::Keypad6; *press = false; break;
        case 0x47: *key = EKey::Keypad7; break;
        case 0xC7: *key = EKey::Keypad7; *press = false; break;
        case 0x48: *key = EKey::Keypad8; break;
        case 0xC8: *key = EKey::Keypad8; *press = false; break;
        case 0x49: *key = EKey::Keypad9; break;
        case 0xC9: *key = EKey::Keypad9; *press = false; break;
        case 0x53: *key = EKey::KeypadPeriod; break;
        case 0xD3: *key = EKey::KeypadPeriod; *press = false; break;
        case 0xE0:
            scan = readScancode();

            switch (scan)
            {
                case 0x10: *key = EKey::Previous; break;
                case 0x90: *key = EKey::Previous; *press = false; break;
                case 0x19: *key = EKey::Next; break;
                case 0x99: *key = EKey::Next; *press = false; break;
                case 0x1C: *key = EKey::KeypadEnter; break;
                case 0x9C: *key = EKey::KeypadEnter; *press = false; break;
                case 0x1D: *key = EKey::RightControl; break;
                case 0x9D: *key = EKey::RightControl; *press = false; break;
                case 0x20: *key = EKey::Mute; break;
                case 0xA0: *key = EKey::Mute; *press = false; break;
                case 0x21: *key = EKey::Calculator; break;
                case 0xA1: *key = EKey::Calculator; *press = false; break;
                case 0x22: *key = EKey::Play; break;
                case 0xA2: *key = EKey::Play; *press = false; break;
                case 0x24: *key = EKey::Stop; break;
                case 0xA4: *key = EKey::Stop; *press = false; break;
                case 0x2E: *key = EKey::VolumeDown; break;
                case 0xAE: *key = EKey::VolumeDown; *press = false; break;
                case 0x30: *key = EKey::VolumeUp; break;
                case 0xB0: *key = EKey::VolumeUp; *press = false; break;
                case 0x32: *key = EKey::WwwHome; break;
                case 0xB2: *key = EKey::WwwHome; *press = false; break;
                case 0x35: *key = EKey::KeypadDivide; break;
                case 0xB5: *key = EKey::KeypadDivide; *press = false; break;
                case 0x38: *key = EKey::RightAlt; break;
                case 0xB8: *key = EKey::RightAlt; *press = false; break;
                case 0x47: *key = EKey::Home; break;
                case 0xC7: *key = EKey::Home; *press = false; break;
                case 0x48: *key = EKey::Up; break;
                case 0xC8: *key = EKey::Up; *press = false; break;
                case 0x49: *key = EKey::PageUp; break;
                case 0xC9: *key = EKey::PageUp; *press = false; break;
                case 0x4B: *key = EKey::Left; break;
                case 0xCB: *key = EKey::Left; *press = false; break;
                case 0x4D: *key = EKey::Right; break;
                case 0xCD: *key = EKey::Right; *press = false; break;
                case 0x4F: *key = EKey::End; break;
                case 0xCF: *key = EKey::End; *press = false; break;
                case 0x50: *key = EKey::Down; break;
                case 0xD0: *key = EKey::Down; *press = false; break;
                case 0x51: *key = EKey::PageDown; break;
                case 0xD1: *key = EKey::PageDown; *press = false; break;
                case 0x52: *key = EKey::Insert; break;
                case 0xD2: *key = EKey::Insert; *press = false; break;
                case 0x53: *key = EKey::Delete; break;
                case 0xD3: *key = EKey::Delete; *press = false; break;
                case 0x5B: *key = EKey::LeftSuper; break;
                case 0xDB: *key = EKey::LeftSuper; *press = false; break;
                case 0x5C: *key = EKey::RightSuper; break;
                case 0xDC: *key = EKey::RightSuper; *press = false; break;
                case 0x65: *key = EKey::WwwSearch; break;
                case 0xE5: *key = EKey::WwwSearch; *press = false; break;
                case 0x66: *key = EKey::WwwFavorites; break;
                case 0xE6: *key = EKey::WwwFavorites; *press = false; break;
                case 0x67: *key = EKey::WwwRefresh; break;
                case 0xE7: *key = EKey::WwwRefresh; *press = false; break;
                case 0x68: *key = EKey::WwwStop; break;
                case 0xE8: *key = EKey::WwwStop; *press = false; break;
                case 0x69: *key = EKey::WwwForward; break;
                case 0xE9: *key = EKey::WwwForward; *press = false; break;
                case 0x6A: *key = EKey::WwwBack; break;
                case 0xEA: *key = EKey::WwwBack; *press = false; break;
                case 0x6B: *key = EKey::MyComputer; break;
                case 0xEB: *key = EKey::MyComputer; *press = false; break;
                case 0x6C: *key = EKey::Email; break;
                case 0xEC: *key = EKey::Email; *press = false; break;
                case 0x6D: *key = EKey::MediaSelect; break;
                case 0xED: *key = EKey::MediaSelect; *press = false; break;

                case 0xB7: *press = false; // print screen released
                case 0x2A: // print screen pressed
                    *key = EKey::PrintScreen; *press = false; break;

                    // read off 2 more bytes
                    for (; i < 2; i++)
                    {
                        readScancode();
                    }
                    break;
            }

            break;
        case 0xE1: // only pause key
            *key = EKey::Pause; *press = false; break;

            // read off 5 more bytes
            for (; i < 5; i++)
            {
                readScancode();
            }

            break;
        default: *key = EKey::None; break;
    }
}

void readScanSet2(EKey* key, bool* press)
{
    byte scan = readScancode();
    byte i = 0;

    switch (scan)
    {
        case 0x76: *key = EKey::Escape; break;
        case 0x05: *key = EKey::F1; break;
        case 0x06: *key = EKey::F2; break;
        case 0x04: *key = EKey::F3; break;
        case 0x0C: *key = EKey::F4; break;
        case 0x03: *key = EKey::F5; break;
        case 0x0B: *key = EKey::F6; break;
        case 0x83: *key = EKey::F7; break;
        case 0x0A: *key = EKey::F8; break;
        case 0x01: *key = EKey::F9; break;
        case 0x09: *key = EKey::F10; break;
        case 0x78: *key = EKey::F11; break;
        case 0x07: *key = EKey::F12; break;
        case 0x0E: *key = EKey::Grave; break;
        case 0x16: *key = EKey::Number1; break;
        case 0x1E: *key = EKey::Number2; break;
        case 0x26: *key = EKey::Number3; break;
        case 0x25: *key = EKey::Number4; break;
        case 0x2E: *key = EKey::Number5; break;
        case 0x36: *key = EKey::Number6; break;
        case 0x3D: *key = EKey::Number7; break;
        case 0x3E: *key = EKey::Number8; break;
        case 0x46: *key = EKey::Number9; break;
        case 0x45: *key = EKey::Number0; break;
        case 0x4E: *key = EKey::Minus; break;
        case 0x55: *key = EKey::Equals; break;
        case 0x66: *key = EKey::Backspace; break;
        case 0x0D: *key = EKey::Tab; break;
        case 0x15: *key = EKey::Q; break;
        case 0x1D: *key = EKey::W; break;
        case 0x24: *key = EKey::E; break;
        case 0x2D: *key = EKey::R; break;
        case 0x2C: *key = EKey::T; break;
        case 0x35: *key = EKey::Y; break;
        case 0x3C: *key = EKey::U; break;
        case 0x43: *key = EKey::I; break;
        case 0x44: *key = EKey::O; break;
        case 0x4D: *key = EKey::P; break;
        case 0x54: *key = EKey::LeftBracket; break;
        case 0x5B: *key = EKey::RightBracket; break;
        case 0x5D: *key = EKey::Backslash; break;
        case 0x58: *key = EKey::CapsLock; break;
        case 0x1C: *key = EKey::A; break;
        case 0x1B: *key = EKey::S; break;
        case 0x23: *key = EKey::D; break;
        case 0x2B: *key = EKey::F; break;
        case 0x34: *key = EKey::G; break;
        case 0x33: *key = EKey::H; break;
        case 0x3B: *key = EKey::J; break;
        case 0x42: *key = EKey::K; break;
        case 0x4B: *key = EKey::L; break;
        case 0x4C: *key = EKey::Semicolon; break;
        case 0x52: *key = EKey::Apostrophe; break;
        case 0x5A: *key = EKey::Enter; break;
        case 0x12: *key = EKey::LeftShift; break;
        case 0x1A: *key = EKey::Z; break;
        case 0x22: *key = EKey::X; break;
        case 0x21: *key = EKey::C; break;
        case 0x2A: *key = EKey::V; break;
        case 0x32: *key = EKey::B; break;
        case 0x31: *key = EKey::N; break;
        case 0x3A: *key = EKey::M; break;
        case 0x41: *key = EKey::Comma; break;
        case 0x49: *key = EKey::Period; break;
        case 0x4A: *key = EKey::Slash; break;
        case 0x59: *key = EKey::RightShift; break;
        case 0x14: *key = EKey::LeftControl; break;
        case 0x11: *key = EKey::LeftAlt; break;
        case 0x29: *key = EKey::Space; break;
        case 0x7E: *key = EKey::ScrollLock; break;
        case 0x77: *key = EKey::NumLock; break;
        case 0x7C: *key = EKey::KeypadMultiply; break;
        case 0x79: *key = EKey::KeypadAdd; break;
        case 0x7B: *key = EKey::KeypadSubtract; break;
        case 0x70: *key = EKey::Keypad0; break;
        case 0x69: *key = EKey::Keypad1; break;
        case 0x72: *key = EKey::Keypad2; break;
        case 0x7A: *key = EKey::Keypad3; break;
        case 0x6B: *key = EKey::Keypad4; break;
        case 0x73: *key = EKey::Keypad5; break;
        case 0x74: *key = EKey::Keypad6; break;
        case 0x6C: *key = EKey::Keypad7; break;
        case 0x75: *key = EKey::Keypad8; break;
        case 0x7D: *key = EKey::Keypad9; break;
        case 0x71: *key = EKey::KeypadPeriod; break;
        case 0xE0:
            scan = readScancode();

            switch (scan)
            {
                case 0x15: *key = EKey::Previous; break;
                case 0x4D: *key = EKey::Next; break;
                case 0x5A: *key = EKey::KeypadEnter; break;
                case 0x14: *key = EKey::RightControl; break;
                case 0x2B: *key = EKey::Calculator; break;
                case 0x34: *key = EKey::Play; break;
                case 0x3B: *key = EKey::Stop; break;
                case 0x23: *key = EKey::Mute; break;
                case 0x21: *key = EKey::VolumeDown; break;
                case 0x32: *key = EKey::VolumeUp; break;
                case 0x3A: *key = EKey::WwwHome; break;
                case 0x4A: *key = EKey::KeypadDivide; break;
                case 0x11: *key = EKey::RightAlt; break;
                case 0x6C: *key = EKey::Home; break;
                case 0x75: *key = EKey::Up; break;
                case 0x7D: *key = EKey::PageUp; break;
                case 0x6B: *key = EKey::Left; break;
                case 0x74: *key = EKey::Right; break;
                case 0x69: *key = EKey::End; break;
                case 0x72: *key = EKey::Down; break;
                case 0x7A: *key = EKey::PageDown; break;
                case 0x70: *key = EKey::Insert; break;
                case 0x71: *key = EKey::Delete; break;
                case 0x1F: *key = EKey::LeftSuper; break;
                case 0x27: *key = EKey::RightSuper; break;
                case 0x10: *key = EKey::WwwSearch; break;
                case 0x18: *key = EKey::WwwFavorites; break;
                case 0x20: *key = EKey::WwwRefresh; break;
                case 0x28: *key = EKey::WwwStop; break;
                case 0x30: *key = EKey::WwwForward; break;
                case 0x38: *key = EKey::WwwBack; break;
                case 0x40: *key = EKey::MyComputer; break;
                case 0x48: *key = EKey::Email; break;
                case 0x50: *key = EKey::MediaSelect; break;
                case 0xF0:
                    scan = readScancode();

                    switch (scan)
                    {
                        case 0x15: *key = EKey::Previous; *press = false; break;
                        case 0x4D: *key = EKey::Next; *press = false; break;
                        case 0x5A: *key = EKey::KeypadEnter; *press = false; break;
                        case 0x14: *key = EKey::RightControl; *press = false; break;
                        case 0x2B: *key = EKey::Calculator; *press = false; break;
                        case 0x34: *key = EKey::Play; *press = false; break;
                        case 0x3B: *key = EKey::Stop; *press = false; break;
                        case 0x23: *key = EKey::Mute; *press = false; break;
                        case 0x21: *key = EKey::VolumeDown; *press = false; break;
                        case 0x32: *key = EKey::VolumeUp; *press = false; break;
                        case 0x3A: *key = EKey::WwwHome; *press = false; break;
                        case 0x4A: *key = EKey::KeypadDivide; *press = false; break;
                        case 0x11: *key = EKey::RightAlt; *press = false; break;
                        case 0x6C: *key = EKey::Home; *press = false; break;
                        case 0x75: *key = EKey::Up; *press = false; break;
                        case 0x7D: *key = EKey::PageUp; *press = false; break;
                        case 0x6B: *key = EKey::Left; *press = false; break;
                        case 0x74: *key = EKey::Right; *press = false; break;
                        case 0x69: *key = EKey::End; *press = false; break;
                        case 0x72: *key = EKey::Down; *press = false; break;
                        case 0x7A: *key = EKey::PageDown; *press = false; break;
                        case 0x70: *key = EKey::Insert; *press = false; break;
                        case 0x71: *key = EKey::Delete; *press = false; break;
                        case 0x1F: *key = EKey::LeftSuper; *press = false; break;
                        case 0x27: *key = EKey::RightSuper; *press = false; break;
                        case 0x10: *key = EKey::WwwSearch; *press = false; break;
                        case 0x18: *key = EKey::WwwFavorites; *press = false; break;
                        case 0x20: *key = EKey::WwwRefresh; *press = false; break;
                        case 0x28: *key = EKey::WwwStop; *press = false; break;
                        case 0x30: *key = EKey::WwwForward; *press = false; break;
                        case 0x38: *key = EKey::WwwBack; *press = false; break;
                        case 0x40: *key = EKey::MyComputer; *press = false; break;
                        case 0x48: *key = EKey::Email; *press = false; break;
                        case 0x50: *key = EKey::MediaSelect; *press = false; break;
                        case 0x7C: // print screen released
                            *key = EKey::PrintScreen; *press = false;

                            // read off 3 more bytes
                            for (; i < 3; i++)
                            {
                                readScancode();
                            }

                            break;
                    }

                    break;
                case 0x12: // print screen pressed
                    *key = EKey::PrintScreen;

                    // read off 2 more bytes
                    for (; i < 2; i++)
                    {
                        readScancode();
                    }

                    break;
            }
            break;
        case 0xF0:
            switch (readScancode())
            {
                case 0x76: *key = EKey::Escape; *press = false; break;
                case 0x05: *key = EKey::F1; *press = false; break;
                case 0x06: *key = EKey::F2; *press = false; break;
                case 0x04: *key = EKey::F3; *press = false; break;
                case 0x0C: *key = EKey::F4; *press = false; break;
                case 0x03: *key = EKey::F5; *press = false; break;
                case 0x0B: *key = EKey::F6; *press = false; break;
                case 0x83: *key = EKey::F7; *press = false; break;
                case 0x0A: *key = EKey::F8; *press = false; break;
                case 0x01: *key = EKey::F9; *press = false; break;
                case 0x09: *key = EKey::F10; *press = false; break;
                case 0x78: *key = EKey::F11; *press = false; break;
                case 0x07: *key = EKey::F12; *press = false; break;
                case 0x0E: *key = EKey::Grave; *press = false; break;
                case 0x16: *key = EKey::Number1; *press = false; break;
                case 0x1E: *key = EKey::Number2; *press = false; break;
                case 0x26: *key = EKey::Number3; *press = false; break;
                case 0x25: *key = EKey::Number4; *press = false; break;
                case 0x2E: *key = EKey::Number5; *press = false; break;
                case 0x36: *key = EKey::Number6; *press = false; break;
                case 0x3D: *key = EKey::Number7; *press = false; break;
                case 0x3E: *key = EKey::Number8; *press = false; break;
                case 0x46: *key = EKey::Number9; *press = false; break;
                case 0x45: *key = EKey::Number0; *press = false; break;
                case 0x4E: *key = EKey::Minus; *press = false; break;
                case 0x55: *key = EKey::Equals; *press = false; break;
                case 0x66: *key = EKey::Backspace; *press = false; break;
                case 0x0D: *key = EKey::Tab; *press = false; break;
                case 0x15: *key = EKey::Q; *press = false; break;
                case 0x1D: *key = EKey::W; *press = false; break;
                case 0x24: *key = EKey::E; *press = false; break;
                case 0x2D: *key = EKey::R; *press = false; break;
                case 0x2C: *key = EKey::T; *press = false; break;
                case 0x35: *key = EKey::Y; *press = false; break;
                case 0x3C: *key = EKey::U; *press = false; break;
                case 0x43: *key = EKey::I; *press = false; break;
                case 0x44: *key = EKey::O; *press = false; break;
                case 0x4D: *key = EKey::P; *press = false; break;
                case 0x54: *key = EKey::LeftBracket; *press = false; break;
                case 0x5B: *key = EKey::RightBracket; *press = false; break;
                case 0x5D: *key = EKey::Backslash; *press = false; break;
                case 0x58: *key = EKey::CapsLock; *press = false; break;
                case 0x1C: *key = EKey::A; *press = false; break;
                case 0x1B: *key = EKey::S; *press = false; break;
                case 0x23: *key = EKey::D; *press = false; break;
                case 0x2B: *key = EKey::F; *press = false; break;
                case 0x34: *key = EKey::G; *press = false; break;
                case 0x33: *key = EKey::H; *press = false; break;
                case 0x3B: *key = EKey::J; *press = false; break;
                case 0x42: *key = EKey::K; *press = false; break;
                case 0x4B: *key = EKey::L; *press = false; break;
                case 0x4C: *key = EKey::Semicolon; *press = false; break;
                case 0x52: *key = EKey::Apostrophe; *press = false; break;
                case 0x5A: *key = EKey::Enter; *press = false; break;
                case 0x12: *key = EKey::LeftShift; *press = false; break;
                case 0x1A: *key = EKey::Z; *press = false; break;
                case 0x22: *key = EKey::X; *press = false; break;
                case 0x21: *key = EKey::C; *press = false; break;
                case 0x2A: *key = EKey::V; *press = false; break;
                case 0x32: *key = EKey::B; *press = false; break;
                case 0x31: *key = EKey::N; *press = false; break;
                case 0x3A: *key = EKey::M; *press = false; break;
                case 0x41: *key = EKey::Comma; *press = false; break;
                case 0x49: *key = EKey::Period; *press = false; break;
                case 0x4A: *key = EKey::Slash; *press = false; break;
                case 0x59: *key = EKey::RightShift; *press = false; break;
                case 0x14: *key = EKey::LeftControl; *press = false; break;
                case 0x11: *key = EKey::LeftAlt; *press = false; break;
                case 0x29: *key = EKey::Space; *press = false; break;
                case 0x7E: *key = EKey::ScrollLock; *press = false; break;
                case 0x77: *key = EKey::NumLock; *press = false; break;
                case 0x7C: *key = EKey::KeypadMultiply; *press = false; break;
                case 0x79: *key = EKey::KeypadAdd; *press = false; break;
                case 0x7B: *key = EKey::KeypadSubtract; *press = false; break;
                case 0x70: *key = EKey::Keypad0; *press = false; break;
                case 0x69: *key = EKey::Keypad1; *press = false; break;
                case 0x72: *key = EKey::Keypad2; *press = false; break;
                case 0x7A: *key = EKey::Keypad3; *press = false; break;
                case 0x6B: *key = EKey::Keypad4; *press = false; break;
                case 0x73: *key = EKey::Keypad5; *press = false; break;
                case 0x74: *key = EKey::Keypad6; *press = false; break;
                case 0x6C: *key = EKey::Keypad7; *press = false; break;
                case 0x75: *key = EKey::Keypad8; *press = false; break;
                case 0x7D: *key = EKey::Keypad9; *press = false; break;
                case 0x71: *key = EKey::KeypadPeriod; *press = false; break;
            }

            break;
        case 0xE1: // only pause key
            *key = EKey::Pause; *press = false; break;

            // read off 7 more bytes
            for (; i < 7; i++)
            {
                readScancode();
            }

            break;
        default:
            break;
    }
}

void readScanSet3(EKey* key, bool* press)
{
    
}

byte readScancode()
{
	byte scan = 0;
	do
	{
		scan = inb(KEY_DATA_PORT);

		if (scan > 0)
		{
			return scan;
		}
	}
	while (true);
}

static bool hasChar(EKey key)
{
    switch (key)
    {
        case EKey::Escape:
        case EKey::F1:
        case EKey::F2:
        case EKey::F3:
        case EKey::F4:
        case EKey::F5:
        case EKey::F6:
        case EKey::F7:
        case EKey::F8:
        case EKey::F9:
        case EKey::F10:
        case EKey::F11:
        case EKey::F12:
        case EKey::CapsLock:
        case EKey::LeftShift:
        case EKey::RightShift:
        case EKey::LeftControl:
        case EKey::LeftSuper:
        case EKey::LeftAlt:
        case EKey::RightAlt:
        case EKey::RightSuper:
        case EKey::RightControl:

        case EKey::PrintScreen:
        case EKey::ScrollLock:
        case EKey::Pause:
        case EKey::Insert:
        case EKey::Home:
        case EKey::PageUp:
        case EKey::Delete:
        case EKey::End:
        case EKey::PageDown:

        case EKey::Up:
        case EKey::Left:
        case EKey::Down:
        case EKey::Right:
        case EKey::NumLock:

        case EKey::Previous:
        case EKey::Next:
        case EKey::Play:
        case EKey::Stop:
        case EKey::VolumeDown:
        case EKey::VolumeUp:
        case EKey::Mute:
        case EKey::WwwHome:
        case EKey::WwwRefresh:
        case EKey::WwwForward:
        case EKey::WwwBack:
        case EKey::WwwSearch:
        case EKey::WwwFavorites:
        case EKey::WwwStop:
        case EKey::MyComputer:
        case EKey::Calculator:
        case EKey::Email:
        case EKey::MediaSelect:
            return false;
        default:
            return true;
    }
}

static char getChar(EKey key)
{
    if (capsLockOn ^ (keystate[EKey::LeftShift] || keystate[EKey::RightShift]))
    {
        return getUppercase(key);
    }
    else
    {
        return getLowercase(key);
    }
}

static char getLowercase(EKey key)
{
    switch (key)
    {
        case Grave: return '`';
        case Number1: return '1';
        case Number2: return '2';
        case Number3: return '3';
        case Number4: return '4';
        case Number5: return '5';
        case Number6: return '6';
        case Number7: return '7';
        case Number8: return '8';
        case Number9: return '9';
        case Number0: return '0';
        case Minus: return '-';
        case Equals: return '=';
        case Backspace: return '\b';
        case Tab: return '\t';
        case Q: return 'q';
        case W: return 'w';
        case E: return 'e';
        case R: return 'r';
        case T: return 't';
        case Y: return 'y';
        case U: return 'u';
        case I: return 'i';
        case O: return 'o';
        case P: return 'p';
        case LeftBracket: return '[';
        case RightBracket: return ']';
        case Backslash: return '\\';
        case A: return 'a';
        case S: return 's';
        case D: return 'd';
        case F: return 'f';
        case G: return 'g';
        case H: return 'h';
        case J: return 'j';
        case K: return 'k';
        case L: return 'l';
        case Semicolon: return ';';
        case Apostrophe: return '\'';
        case Enter: return '\n';
        case Z: return 'z';
        case X: return 'x';
        case C: return 'c';
        case V: return 'v';
        case B: return 'b';
        case N: return 'n';
        case M: return 'm';
        case Comma: return ',';
        case Period: return '.';
        case Slash: return '/';
        case Space: return ' ';
        case KeypadDivide: return '/';
        case KeypadMultiply: return '*';
        case KeypadSubtract: return '-';
        case KeypadAdd: return '+';
        case Keypad0: return '0';
        case Keypad1: return '1';
        case Keypad2: return '2';
        case Keypad3: return '3';
        case Keypad4: return '4';
        case Keypad5: return '5';
        case Keypad6: return '6';
        case Keypad7: return '7';
        case Keypad8: return '8';
        case Keypad9: return '9';
        case KeypadPeriod: return '.';
        case KeypadEnter: return '\n';
        default: return '?';
    }
}

static char getUppercase(EKey key)
{
    switch (key)
    {
        case Grave: return '~';
        case Number1: return '!';
        case Number2: return '@';
        case Number3: return '#';
        case Number4: return '$';
        case Number5: return '%';
        case Number6: return '^';
        case Number7: return '&';
        case Number8: return '*';
        case Number9: return '(';
        case Number0: return ')';
        case Minus: return '_';
        case Equals: return '+';
        case Backspace: return '\b';
        case Tab: return '\t';
        case Q: return 'Q';
        case W: return 'W';
        case E: return 'E';
        case R: return 'R';
        case T: return 'T';
        case Y: return 'Y';
        case U: return 'U';
        case I: return 'I';
        case O: return 'O';
        case P: return 'P';
        case LeftBracket: return '{';
        case RightBracket: return '}';
        case Backslash: return '|';
        case A: return 'A';
        case S: return 'S';
        case D: return 'D';
        case F: return 'F';
        case G: return 'G';
        case H: return 'H';
        case J: return 'J';
        case K: return 'K';
        case L: return 'L';
        case Semicolon: return ':';
        case Apostrophe: return '"';
        case Enter: return '\n';
        case Z: return 'Z';
        case X: return 'X';
        case C: return 'C';
        case V: return 'V';
        case B: return 'B';
        case N: return 'N';
        case M: return 'M';
        case Comma: return '<';
        case Period: return '>';
        case Slash: return '?';
        case Space: return ' ';
        case KeypadDivide: return '/';
        case KeypadMultiply: return '*';
        case KeypadSubtract: return '-';
        case KeypadAdd: return '+';
        case Keypad0: return '0';
        case Keypad1: return '1';
        case Keypad2: return '2';
        case Keypad3: return '3';
        case Keypad4: return '4';
        case Keypad5: return '5';
        case Keypad6: return '6';
        case Keypad7: return '7';
        case Keypad8: return '8';
        case Keypad9: return '9';
        case KeypadPeriod: return '.';
        case KeypadEnter: return '\n';
        default: return '?';
    }
}

static char* getName(EKey key)
{
    switch (key)
    {
        case Escape: return "Escape";
        case F1: return "F1";
        case F2: return "F2";
        case F3: return "F3";
        case F4: return "F4";
        case F5: return "F5";
        case F6: return "F6";
        case F7: return "F7";
        case F8: return "F8";
        case F9: return "F9";
        case F10: return "F10";
        case F11: return "F11";
        case F12: return "F12";
        case Grave: return "Grave";
        case Number1: return "Number1";
        case Number2: return "Number2";
        case Number3: return "Number3";
        case Number4: return "Number4";
        case Number5: return "Number5";
        case Number6: return "Number6";
        case Number7: return "Number7";
        case Number8: return "Number8";
        case Number9: return "Number9";
        case Number0: return "Number0";
        case Minus: return "Minus";
        case Equals: return "Equals";
        case Backspace: return "Backspace";
        case Tab: return "Tab";
        case Q: return "Q";
        case W: return "W";
        case E: return "E";
        case R: return "R";
        case T: return "T";
        case Y: return "Y";
        case U: return "U";
        case I: return "I";
        case O: return "O";
        case P: return "P";
        case LeftBracket: return "LeftBracket";
        case RightBracket: return "RightBracket";
        case Backslash: return "Backslash";
        case CapsLock: return "CapsLock";
        case A: return "A";
        case S: return "S";
        case D: return "D";
        case F: return "F";
        case G: return "G";
        case H: return "H";
        case J: return "J";
        case K: return "K";
        case L: return "L";
        case Semicolon: return "Semicolon";
        case Apostrophe: return "Apostrophe";
        case Enter: return "Enter";
        case LeftShift: return "LeftShift";
        case Z: return "Z";
        case X: return "X";
        case C: return "C";
        case V: return "V";
        case B: return "B";
        case N: return "N";
        case M: return "M";
        case Comma: return "Comma";
        case Period: return "Period";
        case Slash: return "Slash";
        case RightShift: return "RightShift";
        case LeftControl: return "LeftControl";
        case LeftSuper: return "LeftSuper";
        case LeftAlt: return "LeftAlt";
        case Space: return "Space";
        case RightAlt: return "RightAlt";
        case RightSuper: return "RightSuper";
        case RightControl: return "RightControl";

        case PrintScreen: return "PrintScreen";
        case ScrollLock: return "ScrollLock";
        case Pause: return "Pause";
        case Insert: return "Insert";
        case Home: return "Home";
        case PageUp: return "PageUp";
        case Delete: return "Delete";
        case End: return "End";
        case PageDown: return "PageDown";
        case Up: return "Up";
        case Left: return "Left";
        case Down: return "Down";
        case Right: return "Right";

        case NumLock: return "NumLock";
        case KeypadDivide: return "KeypadDivide";
        case KeypadMultiply: return "KeypadMultiply";
        case KeypadSubtract: return "KeypadSubtract";
        case KeypadAdd: return "KeypadAdd";
        case Keypad0: return "Keypad0";
        case Keypad1: return "Keypad1";
        case Keypad2: return "Keypad2";
        case Keypad3: return "Keypad3";
        case Keypad4: return "Keypad4";
        case Keypad5: return "Keypad5";
        case Keypad6: return "Keypad6";
        case Keypad7: return "Keypad7";
        case Keypad8: return "Keypad8";
        case Keypad9: return "Keypad9";
        case KeypadPeriod: return "KeypadPeriod";
        case KeypadEnter: return "KeypadEnter";

        case Previous: return "Previous";
        case Next: return "Next";
        case Play: return "Play";
        case Stop: return "Stop";
        case VolumeDown: return "VolumeDown";
        case VolumeUp: return "VolumeUp";
        case Mute: return "Mute";
        case WwwHome: return "WwwHome";
        case WwwRefresh: return "WwwRefresh";
        case WwwForward: return "WwwForward";
        case WwwBack: return "WwwBack";
        case WwwSearch: return "WwwSearch";
        case WwwFavorites: return "WwwFavorites";
        case WwwStop: return "WwwStop";
        case MyComputer: return "MyComputer";
        case Calculator: return "Calculator";
        case Email: return "Email";
        case MediaSelect: return "MediaSelect";

        default: return "Unknown";
    }
}