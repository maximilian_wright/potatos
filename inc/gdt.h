#ifndef __GDT_H__
#define __GDT_H__

#include "types.h"

#define GDT_NULL_TYPE	0x0
#define GDT_CODE_TYPE	0x9A
#define GDT_DATA_TYPE	0x92

struct GDTEntry
{
	ushort limitLow;
	ushort baseLow;
	byte baseMiddle;
	byte access;
	byte granularity;
	byte baseHigh;
}
__attribute__((packed));

struct GDTPointer
{
	ushort limit;
	uint base;
}
__attribute__((packed));

void setGDTGate(byte index, uint base, uint limit, byte access, byte gran);
void initGDT();

#endif
