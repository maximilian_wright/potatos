CCDIR=~/opt/cross/bin
CC=$(CCDIR)/i686-elf-gcc
CPP=$(CCDIR)/i686-elf-g++
AS=$(CCDIR)/i686-elf-as

SRC_DIR:=src
OBJ_DIR:=obj
INC_DIR:=inc/
OUTDIR:=build
ISODIR:=$(OUTDIR)/isodir
BOOTS:=$(SRC_DIR)/boot.s
BOOTO:=$(patsubst $(SRC_DIR)/%.s,$(OBJ_DIR)/%.ss,$(BOOTS))
MAINFILE:=$(OUTDIR)/main.o
LINKFILE:=$(SRC_DIR)/linker.ld
OSBIN:=$(OUTDIR)/potatos.bin
ISOFILE:=$(OUTDIR)/potatos.iso

DEBUG=
CFLAGS=-ffreestanding -Wall -Wextra -fno-exceptions -fno-rtti -nostartfiles #-O2
LFLAGS=-ffreestanding -nostdlib -lgcc -I$(INC_DIR) #-O2

ASSS:=$(shell find $(SRC_DIR) -name *.s)
OSSS:=$(patsubst $(SRC_DIR)/%.s,$(OBJ_DIR)/%.ss,$(ASSS))
CPPS:=$(shell find $(SRC_DIR) -name *.cpp)
OBJS:=$(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(CPPS))

build: builddir objdir $(OSBIN) isodir $(ISOFILE)

$(ISOFILE): $(OSBIN)
	cp -r $(SRC_DIR)/boot $(ISODIR)
	cp $? $(ISODIR)
	grub-mkrescue -o $@ $(ISODIR)

#$(OSBIN): $(BOOTO) $(MAINFILE)
#$(OSBIN): $(OSSS) $(MAINFILE)
$(OSBIN): $(OSSS) $(OBJS)
	$(CC) -T $(LINKFILE) -o $@ $(LFLAGS) $(OSSS) $(OBJS)

#$(BOOTO): $(BOOTS)
#	$(AS) -o $@ $?

#$(MAINFILE): $(OBJS)
#	ar rvs $@ $?

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	if [ ! -d "$(dir $@)" ]; then \
		mkdir -p $(dir $@); \
	fi
	
	$(CPP) -c $? -o $@ -I$(INC_DIR) $(CFLAGS)

$(OBJ_DIR)/%.ss: $(SRC_DIR)/%.s
	$(AS) -o $@ $?

.PHONY: builddir objdir isodir clean test

clean:
	if [ -d "$(OUTDIR)" ]; then \
		rm -rf $(OUTDIR); \
	fi
	
	if [ -d "$(OBJ_DIR)" ]; then \
		rm -rf $(OBJ_DIR); \
	fi

test:
	echo $(CPPS)
	echo $(OBJS)

builddir:
	if [ ! -d "$(OUTDIR)" ]; then \
		mkdir build; \
	fi

objdir:
	if [ ! -d "$(OBJ_DIR)" ]; then \
		mkdir $(OBJ_DIR); \
	fi

isodir:
	if [ ! -d "$(ISODIR)" ]; then \
		mkdir -p $(ISODIR); \
	fi
