#!/bin/bash -x

# enable color coding
if [ -e "color" ]; then
    . color
fi

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "Unable to parse arguments. `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=o
LONGOPTIONS=offline

PARSED=$(getopt --options=${OPTIONS} --longoptions=${LONGOPTIONS} --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "${PARSED}"

function printHelp() {
    echo "Usage: sudo ./xc.sh <build|clean> [OPTIONS]"
    echo ""
    echo "OPTIONS:="
    echo "    -o,--offline        Enable offline mode. Don't download and use local versions."
    echo ""
}

while true; do
    case "$1" in
        -o|--offline)
            offline=TRUE
            shift
            break
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Unknown argument: $1"
            exit 3
            ;;
    esac
done

if [[ $# -lt 1 ]]; then
    printHelp
    exit 4
fi

if [ "x$offline" == "x" ]; then
    OFFLINE=FALSE
else
    OFFLINE=TRUE
fi

function printStatus() {
    if [ $? -ne 0 ]; then
        echo -e "${RED}FAILED${CLEAR}"
    else
        echo -e "${GREEN}Success${CLEAR}"
    fi
}

export PREFIX="$HOME/opt/cross"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"

function clean() {
    rm -rf build
    rm -rf "$HOME/opt"
}

function build() {
    pushd .

    TEX_TAR=`ls . | grep texinfo`
    M4_TAR=`ls . | grep m4`
    GMP_TAR=`ls . | grep gmp`
    ISL_TAR=`ls . | grep isl`
    BINUTIL_TAR=`ls . | grep binutils`
    GCC_TAR=`ls . | grep gcc`
    MPC_TAR=`ls . | grep mpc`
    MPFR_TAR=`ls . | grep mpfr`
    CLOOG_TAR=`ls . | grep cloog`

    if [ -d "build/" ]; then
        rm -rf build
    fi

    mkdir build

    cp $TEX_TAR build
    cp $M4_TAR build
    cp $GMP_TAR build
    cp $ISL_TAR build
    cp $BINUTIL_TAR build
    cp $GCC_TAR build
    cp $MPC_TAR build
    cp $MPFR_TAR build
    cp $CLOOG_TAR build

    cd build

    tar xf $TEX_TAR
    rm $TEX_TAR
    TEX_DIR=`ls . | grep texinfo`
    pushd .
    cd $TEX_DIR
    ./configure --prefix="$PREFIX" --disable-static
    make
    make check
    make install
    popd

    tar xf $M4_TAR
    rm $M4_TAR
    M4_DIR=`ls . | grep m4`
    pushd .
    cd $M4_DIR
    ./configure --prefix="$PREFIX"
    make
    make install
    popd

    tar xf $GMP_TAR
    rm $GMP_TAR
    GMP_DIR=`ls . | grep gmp`
    pushd .
    cd $GMP_DIR
    ./configure --prefix="$PREFIX"
    make
    make check
    make install
    popd

    tar xf $ISL_TAR
    rm $ISL_TAR
    ISL_DIR=`ls . | grep isl`
    pushd .
    cd $ISL_DIR
    ./configure --prefix="$PREFIX" --with-gmp-prefix="$PREFIX"
    make
    make install
    popd

    tar xf $MPFR_TAR
    rm $MPFR_TAR
    MPFR_DIR=`ls . | grep mpfr`
    pushd .
    cd $MPFR_DIR
    ./configure --prefix="$PREFIX" --with-gmp="$PREFIX"
    make
    make install
    popd

    tar xf $MPC_TAR
    rm $MPC_TAR
    MPC_DIR=`ls . | grep mpc`
    pushd .
    cd $MPC_DIR
    ./configure --prefix="$PREFIX" --with-gmp="$PREFIX" --with-mpfr="$PREFIX"
    make
    make install
    popd

    tar xf $BINUTIL_TAR
    rm $BINUTIL_TAR
    BINUTIL_DIR=`ls . | grep binutils`
    pushd .
    cd $BINUTIL_DIR
    ./configure --target=$TARGET --prefix="$PREFIX" --with-gmp="$PREFIX" --with-isl="$PREFIX" --with-mpfr="$PREFIX" --with-mpc="$PREFIX" --with-sysroot --disable-nls --disable-werror
    make
    make install
    popd

    tar xf $CLOOG_TAR
    rm $CLOOG_TAR
    CLOOG_DIR=`ls . | grep cloog`
    pushd .
    cd $CLOOG_DIR
    ./configure --prefix="$PREFIX" --with-gmp-prefix="$PREFIX" --with-isl-prefix="$PREFIX"
    make
    make install
    popd

    tar xf $GCC_TAR
    rm $GCC_TAR
    GCC_DIR=`ls . | grep gcc`
    pushd .
    cd $GCC_DIR

    ./configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers --with-isl="$PREFIX" --with-cloog="$PREFIX" --with-gmp="$PREFIX" --with-mpfr="$PREFIX" --with-mpc="$PREFIX"
    make all-gcc
    make all-target-libgcc
    make install-gcc
    make install-target-libgcc
    popd

    popd
}

case "$1" in
    build)
        build
        ;;
    clean)
        clean
        ;;
    *)
        echo "Unknown option: $!"
        printHelp
        exit 5
        ;;
esac
