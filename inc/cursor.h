#ifndef __CURSOR_H__
#define __CURSOR_H__

void enableCursor();
void disableCursor();
void setCursorPos(int x, int y);

#endif