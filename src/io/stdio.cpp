#include "io/stdio.h"
#include "vga.h"
#include "util/string.h"
#include "util/num.h"
#include "types.h"
#include "cursor.h"

#include <stdarg.h>

ushort _row = 0;
ushort _column = 0;
byte _textcolor = EVGAColor::LightGrey;
ushort* _buffer = (ushort*)0xB8000;

void initIO()
{
	for (ushort y = 0; y < VGA_HEIGHT; y++)
	{
		for (ushort x = 0; x < VGA_WIDTH; x++)
		{
			const ushort index = y * VGA_WIDTH + x;
			_buffer[index] = vgaEntry('\0', _textcolor);
		}
	}

	enableCursor();
}

void setColor(byte textcolor)
{
	_textcolor = textcolor;
}

void write(const char c)
{
	_putChar(c);
	_updateCursorPos();
}

void write(const char* data)
{
	_write(data, strlen(data));
}

void writef(const char* data, ...)
{
	int len = strlen(data);
	byte numArgs = 0;
	char index[8];

	for (int i = 0; i < len; i++)
	{
		if (data[i] == '%')
		{
			if (i + 1 >= len)
			{
				itoa(i, index);
				writeerr(4, "Missing parameter type after '%' at index ", index, ": ", data);
				return;
			}

			switch (data[i + 1])
			{
				case 'b': // bool
				case 'i': // int
				case 's': // string
				case 'c': // char
				case 'h': // hex
					numArgs++;
					break;
				default:
					itoa(i, index);
					write("[ERR] Unknown format type '%");
					write(data[i + 1]);
					write("' at index ");
					write(index);
					write(": ");
					writeln(data);
					return;
			}
		}
	}

	va_list ap;
	int in;
	char c;
	bool b;
	char value[8];
	char* str;
	va_start(ap, numArgs);

	for (int i = 0; i < len; i++)
	{
		if (data[i] == '%')
		{
			switch (data[i + 1])
			{
				case 'b':
					b = (bool)va_arg(ap, int);
					i++;
					
					if (b)
					{
						write("true");
					}
					else
					{
						write("false");
					}
					break;
				case 'i':
					in = va_arg(ap, int);
					itoa(in, value);
					write(value);
					i++;
					break;
				case 'h':
					in = va_arg(ap, int);
					itoh(in, value);
					write(value);
					i++;
					break;
				case 's':
					str = va_arg(ap, char*);
					write(str);
					i++;
					break;
				case 'c':
				 	c = (char)va_arg(ap, int);
				 	write(c);
				 	i++;
				 	break;
			}
		}
		else
		{
			write(data[i]);
		}
	}

	va_end(ap);
}

void writeln()
{
	_write("\n", 1);
}

void writeln(const char* data)
{
	write(data);
	_write("\n", 1);
}

void writeerr(const char* data)
{
	write("[ERR] ");
	_writeln(data, strlen(data));
}

void writeerr(ushort num, ...)
{
	write("[ERR] ");

	va_list ap;
	va_start(ap, num);

	for (ushort i = 0; i < num; i++)
	{
		const char* str = va_arg(ap, char*);
		write(str);
	}

	va_end(ap);
	writeln();
}

void _write(const char* data, ushort size)
{
	for (ushort i = 0; i < size; i++)
		_putChar(data[i]);

	_updateCursorPos();
}

void _writeln(const char* data, ushort size)
{
	for (ushort i = 0; i < size; i++)
		_putChar(data[i]);
	
	_putChar('\n');
	_updateCursorPos();
}

void _putChar(char c)
{
	if (c == '\n')
	{
		_column = 0;
		_row++;

		if (_row == VGA_HEIGHT)
		{
			_scroll(1);
			_row--;
		}
	}
	else if (c == '\b')
	{
		if (_row == 0)
		{
			if (_column > 0)
			{
				_column--;
				_putEntryAt('\0', _textcolor, _column, _row);
			}
		}
		else if (_column == 0)
		{
			_column = VGA_WIDTH - 1;
			_row--;

			ushort entry = vgaEntry('\0', _textcolor);
			while (entry == _buffer[_row * VGA_WIDTH + _column])
			{
				_column--;
			}

			_column++;
		}
		else
		{
			_column--;
			_putEntryAt('\0', _textcolor, _column, _row);
		}
	}
	else
	{
		_putEntryAt(c, _textcolor, _column, _row);
		
		if (++_column == VGA_WIDTH)
		{
			_column = 0;
			
			if (++_row == VGA_HEIGHT)
			{
				_row = 0;
			}
		}
	}
}

void _putEntryAt(char c, byte color, ushort x, ushort y)
{
	_buffer[y * VGA_WIDTH + x] = vgaEntry(c, color);
}

void _scroll(byte rows)
{
	for (byte i = 0; i < rows; i++)
	{
		for (int r = 0; r < VGA_HEIGHT - 1; r++)
		{
			for (int c = 0; c < VGA_WIDTH; c++)
			{
				_buffer[r * VGA_WIDTH + c] = _buffer[(r + 1) * VGA_WIDTH + c];
			}
		}
	}

	for (int c = 0; c < VGA_WIDTH; c++)
	{
		_buffer[(VGA_HEIGHT - 1) * VGA_WIDTH + c] = vgaEntry(' ', _textcolor);
	}
}

void _updateCursorPos()
{
	setCursorPos(_column, _row);
}