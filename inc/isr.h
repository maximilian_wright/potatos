#ifndef __ISR_H__
#define __ISR_H__

#include "registers.h"

extern "C" void isrHandler(Registers regs);
void registerIsr(byte interrupt, Function handler);

#endif