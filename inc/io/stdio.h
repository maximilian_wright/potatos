#ifndef __STDIO_H__
#define __STDIO_H__

#include "types.h"

extern ushort _row;
extern ushort _column;
extern byte _textcolor;
extern ushort* _buffer;

void initIO();
void setColor(byte textcolor);

void write(const char c);
void write(const char* data);
void writef(const char* data, ...);
void writeln();
void writeln(const char* data);
void writeerr(const char* data);
void writeerr(ushort num, ...);

void _write(const char* data, ushort size);
void _writeln(const char* data, ushort size);
void _putChar(char c);
void _putEntryAt(char c, byte color, ushort x, ushort y);

void _scroll(byte rows = 1);

void _updateCursorPos();

#endif
