#include "util/num.h"
#include "util/string.h"

void itoa(int n, char s[])
{
    int sign = n;

    if (sign < 0)
    {
        n = -n;
    }

    int i = 0;

    do
    {
        s[i++] = n % 10 + '0';
    }
    while ((n /= 10) > 0);

    if (sign < 0)
    {
        s[i++] = '-';
    }

    s[i] = '\0';

    reverse(s);
}

void itoh(uint n, char s[])
{
    int i = 0;
    byte rem;

    do
    {
        rem = n % 16;

        switch (rem)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                s[i++] = rem + '0';
                break;
            case 10:
                s[i++] = 'A';
                break;
            case 11:
                s[i++] = 'B';
                break;
            case 12:
                s[i++] = 'C';
                break;
            case 13:
                s[i++] = 'D';
                break;
            case 14:
                s[i++] = 'E';
                break;
            case 15:
                s[i++] = 'F';
                break;
        }
    }
    while ((n /= 16) > 0);

    s[i] = '\0';

    reverse(s);
}

void itob(int n, char s[])
{
    int sign = n;

    if (sign < 0)
    {
        n = -n;
    }

    int i = 0;

    do
    {
        s[i++] = n % 2 + '0';
    }
    while ((n /= 2) > 0);

    if (sign < 0)
    {
        s[i++] = '-';
    }

    s[i] = '\0';

    reverse(s);
}