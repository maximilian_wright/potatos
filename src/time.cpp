#include "time.h"
#include "irq.h"
#include "idt.h"
#include "io/io.h"
#include "io/stdio.h"
#include "util/num.h"

uint tick = 0;

void initTime()
{
    registerIrq(IRQ0, (Function)irqTick);

    uint divisor = 1193180 / 1000;

    outb(0x43, 0x36);
    outb(0x40, divisor & 0xFF);
    outb(0x40, divisor >> 8);

    enableInterrupt(IRQ0 - 32);
}

void irqTick()
{
    tick++;
    // writef("%i\n", tick);
}

void sleep(uint ms)
{
    uint startTick = tick;
    // writef("startTick: %i\n", startTick);

    while (tick - startTick < ms)
    {
        // writef("%i, %i, %i\n", tick, startTick, ms);
    }
}