#ifndef __MEM_H__
#define __MEM_H__

#include "types.h"

void memset(void* start, byte value, int size);

#endif
