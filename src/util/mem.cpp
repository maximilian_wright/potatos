#include "util/mem.h"

void memset(void* start, byte value, int size)
{
	unsigned char* buffer = (unsigned char*)start;
	
	for (int i = 0; i < size; i++)
	{
		buffer[i] = value;
	}
}