#ifndef __NUM_H__
#define __NUM_H__

#include "types.h"

void itoa(int n, char s[]);
void itoh(uint n, char s[]);
void itob(int n, char s[]);

#endif