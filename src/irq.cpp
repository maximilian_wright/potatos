#include "irq.h"
#include "io/io.h"
#include "io/stdio.h"
#include "util/num.h"

Function irqHandlers[256];

extern "C" void irqHandler(Registers regs)
{
	if (regs.int_no >= 40)
	{
		outb(0xA0, 0x20);
	}

	outb(0x20, 0x20);

	if (regs.int_no != IRQ0 && regs.int_no != IRQ1) // not timer or keyboard
	{
		char int_no[8];
		itoa(regs.int_no, int_no);
		writef("received irq: %i\n", regs.int_no);
	}

	irqHandlers[regs.int_no]();
}

void registerIrq(byte interrupt, const Function handler)
{
	irqHandlers[interrupt] = handler;
}