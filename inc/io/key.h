#ifndef __KEY_H__
#define __KEY_H__

#include "types.h"
#include "ekey.h"
#include "escanset.h"

#define KEY_CMD_PORT 0x64
#define KEY_DATA_PORT 0x60

#define KEY_CMD_LED 0xED
#define LED_SCROLL_LOCK 1
#define LED_NUM_LOCK 2
#define LED_CAPS_LOCK 4

#define KEY_CMD_ECHO 0xEE

#define KEY_CMD_SCANCODE 0xF0
#define SCANCODE_GET 0
#define SCANCODE_SET_1 1
#define SCANCODE_SET_2 2
#define SCANCODE_SET_3 3

#define KEY_CMD_ENABLE_SCAN 0xF4
#define KEY_CMD_DISABLE_SCAN 0xF5

#define KEY_ACK 0xFA
#define KEY_RESEND 0xFE

void initKey();

void disableKeyInterrupts();
void enableKeyInterrupts();
byte getScancodeSet();
void setScancodeSet(EScanSet set);

void irqKeyboard();
void readScanSet1(EKey* key, bool* press);
void readScanSet2(EKey* key, bool* press);
void readScanSet3(EKey* key, bool* press);
byte readScancode();

static bool hasChar(EKey key);
static char getChar(EKey key);
static char getLowercase(EKey key);
static char getUppercase(EKey key);
static char* getName(EKey key);

#endif