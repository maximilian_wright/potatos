#ifndef __TIME_H__
#define __TIME_H__

#include "types.h"

void initTime();
void irqTick();

void sleep(uint ms);

#endif