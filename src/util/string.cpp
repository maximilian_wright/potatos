#include "util/string.h"

int strlen(const char* str)
{
	int len = 0;
	while (str[len])
		len++;
	return len;
}

void reverse(char str[])
{
	int i, j;
	char c;
	for (i = 0, j = strlen(str) - 1; i < j; i++, j--)
	{
		c = str[i];
		str[i] = str[j];
		str[j] = c;
	}
}