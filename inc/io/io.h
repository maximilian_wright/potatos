#ifndef __IO_H__
#define __IO_H__

#include "types.h"

static inline void outb(ushort port, byte value)
{
    asm volatile("outb %0, %1" : : "a"(value), "Nd"(port));
}

static inline byte inb(ushort port)
{
    byte value;

    asm volatile("inb %1, %0" : "=a"(value) : "Nd"(port));

    return value;
}

#endif