#include "cursor.h"
#include "io/io.h"
#include "types.h"
#include "vga.h"

void enableCursor()
{
    outb(0x3D4, 0x0A);
    outb(0x3D5, (inb(0x3D5) & 0xC0) | 0);

    outb(0x3D4, 0x0B);
    outb(0x3D5, (inb(0x3E5) & 0xE0) | 15);
}

void disableCursor()
{
    outb(0x3D4, 0x0A);
    outb(0x3D5, 0x20);
}

void setCursorPos(int x, int y)
{
    ushort pos = y * VGA_WIDTH + x;

    outb(0x3D4, 0x0F);
    outb(0x3D5, (byte)(pos & 0xFF));
    outb(0x3D4, 0x0E);
    outb(0x3D5, (byte)((pos >> 8) & 0xFF));
}