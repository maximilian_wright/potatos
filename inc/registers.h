#ifndef __REGISTERS_H__
#define __REGISTERS_H__

#include "types.h"

struct Registers
{
	uint ds;
	uint edi, esi, ebp, esp, ebx, edx, ecx, eax;
	uint int_no, err_code;
	uint eip, cs, eflags, useresp, ss;
};

#endif