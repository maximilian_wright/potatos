#ifndef __STRING_H__
#define __STRING_H__

#include "types.h"

int strlen(const char* str);
void reverse(char str[]);

#endif
