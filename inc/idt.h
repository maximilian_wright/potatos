#ifndef __IDT_H__
#define __IDT_H__

#include "io/io.h"
#include "types.h"
#include "io/stdio.h"

#define PIC_MASTER_CMD 0x20
#define PIC_MASTER_DATA 0x21
#define PIC_SLAVE_CMD 0xA0
#define PIC_SLAVE_DATA 0xA1

struct IDTEntry
{
	ushort baseLow;
	ushort sel;
	byte zero;
	byte flags;
	ushort baseHigh;
}
__attribute__((packed));

struct IDTPointer
{
	ushort limit;
	uint base;
}
__attribute__((packed));

void setIDTGate(byte index, uint base, ushort sel, byte flags);
void initIDT();

void showMasks();

static inline void disableInterrupts()
{
	asm volatile("cli");
}

static inline void enableInterrupts()
{
    asm volatile("sti");
}

static inline void disableInterrupt(byte irq)
{
	ushort port;

	if (irq < 8)
	{
		port = PIC_MASTER_DATA;
	}
	else
	{
		port = PIC_SLAVE_DATA;
		irq -= 8;
	}

	outb(port, (inb(port) | (1 << irq)));
}

static inline void enableInterrupt(byte irq)
{
	ushort port;

	if (irq < 8)
	{
		port = PIC_MASTER_DATA;
	}
	else
	{
		port = PIC_SLAVE_DATA;
		irq -= 8;
	}

	outb(port, (inb(port) & ~(1 << irq)));
}

#endif