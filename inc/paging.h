#ifndef __PAGING_H__
#define __PAGING_H__

#define PD_PRESENT              1 << 0
#define PD_READ_WRITE           1 << 1
#define PD_USER_SUPERVISOR      1 << 2
#define PD_WRITE_THROUGH        1 << 3
#define PD_CACHE_DISABLED       1 << 4
#define PD_ACCESSED             1 << 5
#define PD_PAGE_SIZE            1 << 6

#define PT_PRESENT              1 << 0
#define PT_READ_WRITE           1 << 1
#define PT_USER_SUPERVISOR      1 << 2
#define PT_WRITE_THROUGH        1 << 3
#define PT_CACHE_DISABLED       1 << 4
#define PT_ACCESSED             1 << 5
#define PT_DIRTY                1 << 6
#define PT_GLOBAL               1 << 7

void initPaging();

#endif