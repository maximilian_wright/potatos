.intel_syntax noprefix

.set ALIGN,	1<<0
.set MEMINFO,	1<<1
.set FLAGS,	ALIGN | MEMINFO
.set MAGIC,	0x1BADB002
.set CHECKSUM, -(MAGIC + FLAGS)

.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM

.section .bss
.align 16
stack_bottom:
.skip 16384
stack_top:

.section .text
.global _start
.type _start, @function
_start:
	mov stack_top, esp

	call kernel_main

	cli
1:	hlt
	jmp 1b

.global flushGDT
flushGDT:
	mov eax, [esp + 4]
	lgdt [eax]
	
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	jmp 0x08:.flush

.flush:
	ret

.global flushIDT
flushIDT:
	mov eax, [esp + 4]
	lidt [eax]
	ret

.size _start, . - _start
