#include "isr.h"
#include "io/io.h"
#include "io/stdio.h"
#include "util/num.h"
#include "io/key.h"

Function isrHandlers[32];

extern "C" void isrHandler(Registers regs)
{
	switch (regs.int_no)
	{
		case 0:
			writeln("[ISR] Divide by zero exception");
			break;
		case 1:
			writeln("[ISR] Debug exception");
			break;
		case 2:
			writeln("[ISR] Non maskable interrupt");
			break;
		case 3:
			writeln("[ISR] Breakpoint exception");
			break;
		case 4:
			writeln("[ISR] Into detected overflow");
			break;
		case 5:
			writeln("[ISR] Out of bounds exception");
			break;
		case 6:
			writeln("[ISR] Invalid opcode exception");
			break;
		case 7:
			writeln("[ISR] No coprocessor exception");
			break;
		case 8:
			writef("[ISR] Double fault: %i\n", regs.err_code);
			break;
		case 9:
			writeln("[ISR] Coprocessor segment overrun");
			break;
		case 10:
			writef("[ISR] Bad TSS: %i\n", regs.err_code);
			break;
		case 11:
			writef("[ISR] Segment not present: %i\n", regs.err_code);
			break;
		case 12:
			writef("[ISR] Stack fault: %i\n", regs.err_code);
			break;
		case 13:
			writef("[ISR] General protection fault: %i\n", regs.err_code);
			break;
		case 14:
			writef("[ISR] Page fault: %i\n", regs.err_code);
			break;
		case 15:
			writeln("[ISR] Unknown interrupt exception");
			break;
		case 16:
			writeln("[ISR] Coprocessor fault");
			break;
		case 17:
			writeln("[ISR] Alignment check exception");
			break;
		case 18:
			writeln("[ISR] Machine check exception");
			break;
		default:
			writef("[ISR] %i\n", regs.int_no);
			break;
	}

	isrHandlers[regs.int_no]();

	writef("DS        %h\n", regs.ds);
	writef("EDI       %h\n", regs.edi);
	writef("ESI       %h\n", regs.esi);
	writef("EBP       %h\n", regs.ebp);
	writef("ESP       %h\n", regs.esp);
	writef("EBX       %h\n", regs.ebx);
	writef("EDX       %h\n", regs.edx);
	writef("ECX       %h\n", regs.ecx);
	writef("EAX       %h\n", regs.eax);
	writeln();
	writef("EIP       %h\n", regs.eip);
	writef("CS        %h\n", regs.cs);
	writef("EFLAGS    %h\n", regs.eflags);
	writef("USERESP   %h\n", regs.useresp);
	writef("SS        %h\n", regs.ss);

	asm("hlt");
}

void registerIsr(byte interrupt, const Function handler)
{
	isrHandlers[interrupt] = handler;
}
