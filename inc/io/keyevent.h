#ifndef __KEYEVENT_H__
#define __KEYEVENT_H__

#include "ekey.h"

struct KeyEvent
{
    EKey key;
    bool pressed;
};

#endif