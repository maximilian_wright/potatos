#include "gdt.h"

GDTEntry gdt[3];
GDTPointer gdtp;

extern "C" void flushGDT(uint);

void setGDTGate(byte index, uint base, uint limit, byte access, byte gran)
{
	gdt[index].baseLow = base & 0xFFFF;
	gdt[index].baseMiddle = (base >> 16) & 0xFF;
	gdt[index].baseHigh = (base >> 24) & 0xFF;
	
	gdt[index].limitLow = limit & 0xFFFF;
	gdt[index].granularity = (limit >> 16) & 0xF;
	
	gdt[index].granularity |= gran & 0xF0;
	gdt[index].access = access;
}

void initGDT()
{
	gdtp.limit = sizeof(GDTEntry) * 3 - 1;
	gdtp.base = (uint)&gdt;
	
	setGDTGate(0, 0, 0, GDT_NULL_TYPE, 0); // null segment
	setGDTGate(1, 0, 0xFFFFFFFF, GDT_CODE_TYPE, 0xCF); // code segment
	setGDTGate(2, 0, 0xFFFFFFFF, GDT_DATA_TYPE, 0xCF); // data segment
	
	flushGDT((uint)&gdtp);
}