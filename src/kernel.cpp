#include "kernel.h"

#if !defined(__cplusplus)
#include <stdbool.h>
#endif

#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble."
#endif

#if !defined(__i386__)
#error "This tutorial needs to be compiled with an ix86-elf compiler!"
#endif

#include "gdt.h"
#include "idt.h"
#include "paging.h"
#include "io/stdio.h"
#include "io/key.h"
#include "time.h"

extern "C" void kernel_main()
{
	disableInterrupts();

	initIO();
	initGDT();
	initIDT();
	initPaging();

	enableInterrupts();

	initTime();
	initKey();
	
	writeln("Welcome to PotatOS!");

	asm volatile("int $0x14");

	for(;;) {
		asm("hlt");
	}
}
